//
//  ViewController.swift
//  Random_Movie
//
//  Created by Claudia Catapano on 05/12/2019.
//  Copyright © 2019 Claudia Catapano. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var titolo: UILabel!
    @IBOutlet weak var descriptionMovie: UILabel!
    @IBOutlet weak var movieRating: UILabel!
    @IBOutlet weak var infoMovie: UILabel!
    @IBOutlet weak var movieBackground: UIImageView!
    @IBOutlet weak var gradientScrollView: UIScrollView!
    @IBOutlet weak var favouriteButton: UIButton!
    var indexFilm: Int = 0
    
    
    //COLLECTION VIEW SETTING
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(ActorCell.self, forCellWithReuseIdentifier: "cell")
        return cv
    }()
    
    func initCollectionView() {
    }
    
    //ADD TO FAVORITES
    @IBAction func favButtonTapped(sender: UIButton) {
        movieArray[indexFilm].isFavorite = !movieArray[indexFilm].isFavorite
        changeFavoriteButton(movieArray[indexFilm].isFavorite)
    }
    func changeFavoriteButton(_ isFavorite: Bool) {
        if isFavorite {
            let image = UIImage(named: "star.png")
            favouriteButton.setImage(image, for: .normal)
        } else {
            let image = UIImage(named: "starPressed.png")
            favouriteButton.setImage(image, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //CONSTRAINTS COLLECTION VIEW & DELEGATE
        collectionView.delegate = self
        collectionView.dataSource = self
        
        initCollectionView()
        view.addSubview(collectionView)
        collectionView.backgroundColor = .clear
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -90).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -70).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40).isActive = true
        collectionView.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        changeBackground(prevInt: indexFilm)
        
        // Do any additional setup after loading the view.
    }
    
    //HIDE NAVIGATION BAR IN THE CURRENT VIEW
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    //ANIMATE BACKGROUND AND CHANGE MOVIE
    @IBAction func random(_ sender: Any) {
        changeBackground(prevInt: indexFilm)
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            changeBackground(prevInt: indexFilm)
        }
    }
    
    
    
    
    func changeBackground(prevInt: Int) {
        UIView.animate(withDuration: 0.3, animations: {
            self.movieBackground.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }) { (success) in
            self.movieBackground.image = movieArray[self.indexFilm].imageMovie
            self.titolo.text = movieArray[self.indexFilm].titleMovie
            self.descriptionMovie.text = movieArray[self.indexFilm].descriptionMovie
            self.movieRating.text = movieArray[self.indexFilm].ratingMovie
            self.infoMovie.text = movieArray[self.indexFilm].infoMovie
            self.changeFavoriteButton(movieArray[self.indexFilm].isFavorite)
            self.collectionView.reloadData()
            
            UIView.animate(withDuration: 0.5, animations: {
                self.movieBackground.transform = .identity
            }, completion: nil)
        }
        
        //RANDOM ACTION
    
        indexFilm = Int.random(in: 0...18)
        while indexFilm == prevInt {
            indexFilm = Int.random(in: 0...18)
//        indexFilm = 1
        }
        print(indexFilm)
        
        //SCROLL VIEW
        gradientScrollView.contentSize = CGSize(width: 0, height: 800)
        self.gradientScrollView.layer.cornerRadius = 20.0
        self.gradientScrollView.clipsToBounds = true
        self.movieBackground.layer.cornerRadius = 20.0
        self.movieBackground.clipsToBounds = true
    }
    

}

extension ViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.5, height: collectionView.frame.width / 3)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieArray[indexFilm].cast.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ActorCell
        

        
        cell.setActor(actor: movieArray[indexFilm].cast[indexPath.row])
        cell.initCell(
            size: CGSize(
                width:  collectionView.frame.width / 3.6,
                height: collectionView.frame.width / 2.9
            )
        )
//        cell.actor = movieArray[indexFilm].cast[indexPath.row]
//        cell.actorName.text  = movieArray[indexFilm].cast[indexPath.row].name
//        cell.actorFoto.image = movieArray[indexFilm].cast[indexPath.row].image
        //
        return cell
    }
}

