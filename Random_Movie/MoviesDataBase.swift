//
//  actors.swift
//  Random_Movie
//
//  Created by Claudia Catapano on 06/12/2019.
//  Copyright © 2019 Claudia Catapano. All rights reserved.
//

import Foundation
import UIKit


struct Actor {
    var name: String;
    var image: UIImage;
}

struct Movies {
    var titleMovie : String
    var imageMovie : UIImage
    var infoMovie : String
    var ratingMovie : String
    var descriptionMovie : String
    var isFavorite : Bool
    var cast: [Actor]!
}

var movie1 = Movies(
    titleMovie: "Inglorious Bastards",
    imageMovie: UIImage(named: "INGBEST")!,
    infoMovie: "2h 33min | Adventure, Drama, War | 2009",
    ratingMovie: "8.3/10",
    descriptionMovie: "In Nazi-occupied France during World War II, a plan to assassinate Nazi leaders by a group of Jewish U.S. soldiers coincides with a theatre owner's vengeful plans for the same.", isFavorite: true ,
    cast: [
       Actor(name: " Michael Fassbender ", image: #imageLiteral(resourceName: "FASS")),
       Actor(name: " Christoph Waltz ", image:  #imageLiteral(resourceName: "CHRIST-1")),
       Actor(name: " Melanie Laurent ", image:  #imageLiteral(resourceName: "MEL")),
       Actor(name: " Brad Pitt ", image: #imageLiteral(resourceName: "BRADD"))
    ]
    
)
var movie2 = Movies(
    titleMovie: "Silver Linings Playbook",
    imageMovie: UIImage(named: "SilverLinings")!,
    infoMovie: "2h 2min | Comedy, Drama, Romance | 2012",
    ratingMovie: "7.7/10",
    descriptionMovie: "After a stint in a mental institution, former teacher Pat Solitano moves back in with his parents and tries to reconcile with his ex-wife. Things get more challenging when Pat meets Tiffany, a mysterious girl with problems of her own.", isFavorite: true,
    cast: [
       Actor(name: " Bradley Cooper ", image: #imageLiteral(resourceName: "COOPER")),
       Actor(name: " Jennifer Lawrence ", image:  #imageLiteral(resourceName: "JENNIFER")),
       Actor(name: " Robert De Niro ", image:  #imageLiteral(resourceName: "ROBERT")),
       Actor(name: " Jacki Weaver ", image: #imageLiteral(resourceName: "JACKIE"))
    ]
)


var movie3 = Movies(
    titleMovie: "The Royal Tenenbaums",
    imageMovie: UIImage(named: "TENENBAUM")!,
    infoMovie: "1h 50min | Comedy, Drama | 2002",
    ratingMovie: "7.6/10",
    descriptionMovie: "The eccentric members of a dysfunctional family reluctantly gather under the same roof for various reasons.", isFavorite: true,
    cast: [
       Actor(name: " Owen Wilson ", image: #imageLiteral(resourceName: "OWEN")),
       Actor(name: " Ben Stiller ", image:  #imageLiteral(resourceName: "BEN")),
       Actor(name: " Bill Murray ", image:  #imageLiteral(resourceName: "BILL")),
       Actor(name: " Gwyneth Paltrow ", image: #imageLiteral(resourceName: "GWEN"))
    ]
)

var movie4 = Movies(
titleMovie: "Call Me By Your Name",
imageMovie: UIImage(named: "CALLMEBYYOURNAME")!,
infoMovie: "1h 50min | Comedy, Drama | 2002",
ratingMovie: "7.6/10",
descriptionMovie: "In 1980s Italy, a romance blossoms between a seventeen-year-old student and the older man hired as his father's research assistant.",
isFavorite: true,
cast: [
   Actor(name: " Armie Hammer ", image: #imageLiteral(resourceName: "ARMIE")),
   Actor(name: " Timothée Chalamet ", image:  #imageLiteral(resourceName: "TIMOTEE")),
   Actor(name: " Michael Stuhlbarg ", image:  #imageLiteral(resourceName: "MICHEAEL")),
   Actor(name: " Amira Casar ", image: #imageLiteral(resourceName: "AMIRA"))
]
)

var movie5 = Movies(
    titleMovie: "The Big Lebowski",
    imageMovie: UIImage(named: "BIG")!,
    infoMovie:  "1h 57min | Comedy, Crime, Sport | 1998",
    ratingMovie: "8.1/10",
    descriptionMovie: "Jeff ,The Dude, Lebowski, mistaken for a millionaire of the same name, seeks restitution for his ruined rug and enlists his bowling buddies to help get it." ,
    isFavorite: true, cast: [
   Actor(name: " Jeff Bridges ", image: #imageLiteral(resourceName: "JEFF")),
   Actor(name: " John Goodman ", image:  #imageLiteral(resourceName: "JON")),
   Actor(name: " Julianne Moore ", image:  #imageLiteral(resourceName: "JULIEN")),
   Actor(name: " Steve Buscemi ", image: #imageLiteral(resourceName: "STEVER"))
])

var movie6 = Movies(
    titleMovie: "Three Billboards Outside Ebbing, Missouri",
    imageMovie: UIImage(named: "EBBING")!,
    infoMovie: " 1h 55min | Comedy, Crime, Drama | 2017", ratingMovie: "8.2/10",
    descriptionMovie: "A mother personally challenges the local authorities to solve her daughter's murder when they fail to catch the culprit.",
    isFavorite: true , cast:  [
   Actor(name: " Frances McDormand ", image: #imageLiteral(resourceName: "FRANCES")),
   Actor(name: " Caleb Landry Jones ", image:  #imageLiteral(resourceName: "CALEB")),
   Actor(name: " Kerry Condon ", image:  #imageLiteral(resourceName: "KERRY")),
   Actor(name: " Sam Rockwell ", image: #imageLiteral(resourceName: "SAM"))
])

var movie7 = Movies(
    titleMovie: "Her",
    imageMovie: UIImage(named: "HER")!,
    infoMovie: " 2h 6min | Drama, Romance, Sci-Fi | 2014", ratingMovie: "8.0/10",
    descriptionMovie: "In a near future, a lonely writer develops an unlikely relationship with an operating system designed to meet his every need.",
    isFavorite: true , cast:  [
   Actor(name: " Joaquin Phoenix ", image: #imageLiteral(resourceName: "JOAQUIN")),
   Actor(name: " Scarlett Johansson ", image:  #imageLiteral(resourceName: "SCARLETT")),
   Actor(name: " Rooney Mara ", image:  #imageLiteral(resourceName: "ROONEY")),
   Actor(name: " Amy Adams ", image: #imageLiteral(resourceName: "AMY"))
])

var movie8 = Movies(
    titleMovie: "Beginners",
    imageMovie: UIImage(named: "BEGINNERS")!,
    infoMovie: "1h 45min | Comedy, Drama, Romance | 2011", ratingMovie: "7.2/10",
    descriptionMovie: "A young man is rocked by two announcements from his elderly father: that he has terminal cancer and that he has a young male lover.",
    isFavorite: true , cast:  [
   Actor(name: " Ewan McGregor ", image: #imageLiteral(resourceName: "EWAN")),
   Actor(name: " Christopher Plummer ", image:  #imageLiteral(resourceName: "CHRIST")),
   Actor(name: " Mélanie Laurent ", image:  #imageLiteral(resourceName: "MEL")),
   Actor(name: " Goran Visnjic ", image: #imageLiteral(resourceName: "GORAN"))
])

var movie9 = Movies(
    titleMovie: "Midnight In Paris",
    imageMovie: UIImage(named: "MIDNIGHT")!,
    infoMovie: " 1h 34min | Comedy, Fantasy, Romance | 2011",
    ratingMovie: "7.7/10",
    descriptionMovie: "While on a trip to Paris with his fiancée's family, a nostalgic screenwriter finds himself mysteriously going back to the 1920s everyday at midnight.",
    isFavorite: true , cast:  [
   Actor(name: " Owen Wilson ", image: #imageLiteral(resourceName: "OWEN")),
   Actor(name: " Rachel McAdams ", image:  #imageLiteral(resourceName: "RACHEL")),
   Actor(name: " Kurt Fuller ", image:  #imageLiteral(resourceName: "KURT")),
   Actor(name: " Adrien Brody ", image: #imageLiteral(resourceName: "COOPER"))
])

var movie10 = Movies(
    titleMovie: "Inception",
    imageMovie: UIImage(named: "INCEPTION")!,
    infoMovie: "2h 28min | Action, Adventure, Sci-Fi | 2010",
    ratingMovie: "8.8/10",
    descriptionMovie: "A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a C.E.O.",
    isFavorite: true , cast:  [
   Actor(name: " Leonardo DiCaprio ", image: #imageLiteral(resourceName: "LEO")),
   Actor(name: " Joseph L. Levitt ", image:  #imageLiteral(resourceName: "LEVITT")),
   Actor(name: " Ellen Page", image:  #imageLiteral(resourceName: "ELLEN")),
   Actor(name: " Tom Hardy ", image: #imageLiteral(resourceName: "TOMHARDY"))
])


var movie11 = Movies(
    titleMovie: "Shutter Island",
    imageMovie: UIImage(named: "SHUTTER")!,
    infoMovie: "2h 18min | Mystery, Thriller | 2010",
    ratingMovie: "8.1/10",
    descriptionMovie: "In 1954, a U.S. Marshal investigates the disappearance of a murderer who escaped from a hospital for the criminally insane.",
    isFavorite: true , cast:  [
   Actor(name: " Leonardo DiCaprio ", image: #imageLiteral(resourceName: "LEO")),
   Actor(name: " Mark Ruffalo ", image:  #imageLiteral(resourceName: "MARK")),
   Actor(name: " Ben Kingsley ", image:  #imageLiteral(resourceName: "BENK")),
   Actor(name: " Michelle Williams ", image: #imageLiteral(resourceName: "MICHELLE"))
])


var movie12 = Movies(
    titleMovie: "Léon: The Professional",
    imageMovie: UIImage(named: "LEON")!,
    infoMovie: "1h 50min | Action, Crime, Drama | 1994",
    ratingMovie: "8.5/10",
    descriptionMovie: "Mathilda, a 12-year-old girl, is reluctantly taken in by Léon, a professional assassin, after her family is murdered. An unusual relationship forms as she becomes his protégée and learns the assassin's trade.", isFavorite: true,
    cast: [
       Actor(name: " Jean Reno ", image: #imageLiteral(resourceName: "RENO")),
       Actor(name: " Gary Oldman ", image:  #imageLiteral(resourceName: "GARY")),
       Actor(name: " Natalie Portman ", image:  #imageLiteral(resourceName: "NAT")),
       Actor(name: " Danny Aiello ", image: #imageLiteral(resourceName: "DANNY"))
    ]
)

var movie13 = Movies(
    titleMovie: "Eternal Sunshine of the Spotless Mind",
    imageMovie: UIImage(named: "ETERNAL")!,
    infoMovie: " 1h 48min | Drama, Romance, Sci-Fi | 2004 ",
    ratingMovie: "8.3/10",
    descriptionMovie: "When their relationship turns sour, a couple undergoes a medical procedure to have each other erased from their memories.", isFavorite: true,
    cast: [
       Actor(name: " Jim Carrey ", image: #imageLiteral(resourceName: "JIM")),
       Actor(name: " Kate Winslet ", image:  #imageLiteral(resourceName: "KATE")),
       Actor(name: " Elijah Wood ", image:  #imageLiteral(resourceName: "FRODO")),
       Actor(name: " Mark Ruffalo ", image: #imageLiteral(resourceName: "MARK"))
    ]
)

var movie14 = Movies(
    titleMovie: "Mommy",
    imageMovie: UIImage(named: "MOMMY")!,
    infoMovie: " 1h 48min | Drama, Romance, Sci-Fi | 2004 ",
    ratingMovie: "8.3/10",
    descriptionMovie: "When their relationship turns sour, a couple undergoes a medical procedure to have each other erased from their memories.", isFavorite: true,
    cast: [
       Actor(name: " Anne Dorval ", image: #imageLiteral(resourceName: "ANNE")),
       Actor(name: " Suzanne Clément ", image:  #imageLiteral(resourceName: "SUZ")),
       Actor(name: " Antoine O. Pilon ", image:  #imageLiteral(resourceName: "ANTO"))
    ]
)

var movie15 = Movies(
    titleMovie: "Match Point",
    imageMovie: UIImage(named: "MATCHPOINT")!,
    infoMovie: " 2h 4min | Drama, Romance, Thriller | 2006 ",
    ratingMovie: "7.3/10",
    descriptionMovie: "At a turning point in his life, a former tennis pro falls for an actress who happens to be dating his friend and soon-to-be brother-in-law."
, isFavorite: true,
    cast: [
       Actor(name: " Jonathan Rhys Meyers ", image: #imageLiteral(resourceName: "JonathanRhysMeyers")),
       Actor(name: " Alexander Armstrong ", image:  #imageLiteral(resourceName: "AlexanderArmstrong")),
       Actor(name: " Matthew Goode ", image:  #imageLiteral(resourceName: "Matthew Goode_")),
         Actor(name: " Scarlett Johansson ", image:  #imageLiteral(resourceName: "SCARLETT"))
    ]
)


var movie16 = Movies(
    titleMovie: "Dogville",
    imageMovie: UIImage(named: "DOGVILLE")!,
    infoMovie: " 2h 15min | Crime, Drama | 2004 ",
    ratingMovie: "8.0/10",
    descriptionMovie: "A woman on the run from the mob is reluctantly accepted in a small Colorado town. In exchange, she agrees to work for them. As a search visits the town, she finds out that their support has a price. Yet her dangerous secret is never far away."
, isFavorite: true,
    cast: [
       Actor(name: " Nicole Kidman ", image: #imageLiteral(resourceName: "NICOLE")),
       Actor(name: " Harriet Andersson ", image:  #imageLiteral(resourceName: "HARRIETT")),
       Actor(name: " Lauren Bacall ", image:  #imageLiteral(resourceName: "LAUREN")),
         Actor(name: " Jean-Marc Barr ", image:  #imageLiteral(resourceName: "JEAN"))
    ]
)

var movie17 = Movies(
    titleMovie: "Little Miss Sunshine",
    imageMovie: UIImage(named: "MISSSUNSHINE")!,
    infoMovie: " 1h 41min | Comedy, Drama | 2006  ",
    ratingMovie: "7.8/10",
    descriptionMovie: "A family determined to get their young daughter into the finals of a beauty pageant take a cross-country trip in their VW bus."
, isFavorite: true,
    cast: [
       Actor(name: " Abigail Breslin ", image: #imageLiteral(resourceName: "ABIGAIL")),
       Actor(name: " Paul Dano ", image:  #imageLiteral(resourceName: "PAUL")),
       Actor(name: " Steve Carell ", image:  #imageLiteral(resourceName: "STEVEC")),
         Actor(name: " Toni Collette ", image:  #imageLiteral(resourceName: "TONI"))
    ]
)


var movie18 = Movies(
    titleMovie: "Django Unchained",
    imageMovie: UIImage(named: "DJANGO")!,
    infoMovie: " 2h 45min | Drama, Western |  2012 ",
    ratingMovie: "8.4/10",
    descriptionMovie: "With the help of a German bounty hunter, a freed slave sets out to rescue his wife from a brutal Mississippi plantation owner."
, isFavorite: true,
    cast: [
       Actor(name: " Jamie Foxx ", image: #imageLiteral(resourceName: "FOXX")),
       Actor(name: " Christoph Waltz ", image:  #imageLiteral(resourceName: "CHRIST-1")),
       Actor(name: " Leonardo DiCaprio ", image:  #imageLiteral(resourceName: "LEO")),
         Actor(name: " Kerry Washington ", image:  #imageLiteral(resourceName: "KERRYW"))
    ]
)

var movie19 = Movies(
    titleMovie: "One Flew Over the Cuckoo's Nest",
    imageMovie: UIImage(named: "QUALCUNO")!,
    infoMovie: "  2h 13min | Drama | 1975 ",
    ratingMovie: "8.7/10",
    descriptionMovie: "A criminal pleads insanity and is admitted to a mental institution, where he rebels against the oppressive nurse and rallies up the scared patients."
, isFavorite: true,
    cast: [
       Actor(name: " Jack Nicholson ", image: #imageLiteral(resourceName: "JACKN")),
       Actor(name: " Danny DeVito ", image:  #imageLiteral(resourceName: "DANNYDV")),
       Actor(name: " Will Sampson ", image:  #imageLiteral(resourceName: "WILLS")),
     
    ]
)



var movieArray = [movie1, movie2, movie3, movie4, movie5, movie6, movie7, movie8, movie9, movie10, movie11, movie12, movie13, movie14, movie15, movie16, movie17, movie18, movie19]


