//
//  ActorCollectionViewCell.swift
//  Random_Movie
//
//  Created by Claudia Catapano on 12/12/2019.
//  Copyright © 2019 Claudia Catapano. All rights reserved.
//

import UIKit


//CAST SETTING
class ActorCell: UICollectionViewCell {
    
    var actor: Actor? = nil

    var actorFoto: UIImageView!
    var actorName: UILabel!

    func initCell(size: CGSize) {
        let view = UIView(
            frame: CGRect(
                x: 0,
                y: 0,
                width: size.width,
                height: size.height
            )
        )
        let labelView = UITextView(
            frame: CGRect(
                x: 0,
                y: 0,
                width: size.width,
                height: 80
            )
        )
        
        actorFoto.frame = CGRect(
            x: 0,
            y: 0,
            width: size.width,
            height: size.height
        )
        actorName.frame = CGRect(x:0, y: 40, width:size.width, height: 100)
//        actorName.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5485605736)
        


        actorName.adjustsFontSizeToFitWidth = true
        
        
        actorName.textColor = .white
        view.addSubview(actorFoto)
        view.addSubview(actorName)

//        view.addSubview(labelView)
        

        
        view.clipsToBounds = true
        view.layer.cornerRadius = 10.0
        self.addSubview(view)
    }


    public func setActor(actor: Actor) {
        actorFoto = UIImageView()
        actorFoto.image = actor.image
        actorName = UILabel()
        actorName.text = actor.name

        
        
        self.actor = actor
        
    }
    
    
//    actorFoto.image = actor.image
    
}
