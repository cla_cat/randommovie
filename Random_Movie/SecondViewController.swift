//
//  SecondViewController.swift
//  Random_Movie
//
//  Created by Claudia Catapano on 09/12/2019.
//  Copyright © 2019 Claudia Catapano. All rights reserved.
//

import UIKit

class SecondViewController : UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
    }
}
